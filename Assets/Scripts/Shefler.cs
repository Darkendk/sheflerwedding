﻿using UnityEngine;
using System.Collections;
using System;

public class Shefler : Singleton<Shefler>
{
	[System.NonSerialized]
	public int numberOfClks = 0;
	[System.NonSerialized]
	public int numberOfClksNeeded = 0;

	[System.NonSerialized]
	public Rigidbody2D shef_Rigid;
	bool swiping;
	Vector3 firstPosition;
	public float shef_Speed = -5;
	public bool canMove = true;
	 
	StopSign currentStopSignScript;
	TextDisplay uiText;
	public float teleportDistance = 2;
	public int minClicks = 3;
	public int maxClicks = 7;
	public ScreenShaker screenshake;
	public event Action SignGrabbed;
	public event Action SignDestroied;
	public event Action Proposed;
	public event Action TriedToMove;
	public GameObject proposedVFX;
	public AudioClip sparkle;

	SpriteRenderer mySpriteMainRend; // My Sptite
	public Sprite propose1;
	public Sprite propose2;
	// Screens 
	public GameObject GameOver;
	public GameObject YouWin;
	public bool gameoverbool = false;
	// Use this for initialization
	void Start ()
	{
		shef_Rigid = gameObject.GetComponent<Rigidbody2D> ();
		uiText = gameObject.GetComponent<TextDisplay> ();
		mySpriteMainRend = gameObject.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if  (gameoverbool == false)
		{
			if(canMove == true)
			{
				if (Input.GetMouseButton(0))
					{
						if (swiping == false)
					{
						swiping = true;
						firstPosition = Input.mousePosition;
						return;
					}
					else
					{
						Vector3 direction = Input.mousePosition - firstPosition;
						if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
						{
							if (direction.x > 0) 
							{
								
							}
							else
							{
								MoveLeft();
							}
						}
					}
					swiping = false;
				}
			}
			else
			{
				if (Input.GetMouseButtonDown(0))
				{
					TriedToMove();
					numberOfClks++;
					uiText.UpdateText();
					if(numberOfClks == numberOfClksNeeded)
					{
						DestroySign();
					}
					currentStopSignScript.HoldSFX();
				}
			}
		}

		//Cheat Code For Debuging
		if(Input.GetKeyDown(KeyCode.Keypad1))
		{
			gameObject.transform.position = new Vector2(-(gameObject.transform.position.x),gameObject.transform.position.y);                                         
		}
	}
	void MoveLeft()
	{
		shef_Rigid.velocity += new Vector2(shef_Speed*Time.deltaTime,0);
	}


	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.tag == "Yam" && gameoverbool == false)
			{

				Yam yamscript = col.gameObject.GetComponent<Yam>();
				if(yamscript.visable == false)
				{
					GameOver.SetActive(true);
					yamscript.YouLose();
				}
			else if(yamscript.visable == true)
				{
					
					mySpriteMainRend.sprite = propose1;
					Invoke ("StartCinematic",2.5f);
					shef_Rigid.velocity = Vector2.zero;
					Proposed();
					print ("you win");
					yamscript.YouWin();
				}
				gameoverbool = true;
			}
	}

	void DestroySign()
	{
		SignDestroied ();
		numberOfClks = 0;
		screenshake.Shake(0.2f,0.3f);
		currentStopSignScript.DestroyMe();
		uiText.ResetText();
		canMove = true;
		gameObject.transform.position = new Vector3 (gameObject.transform.position.x - teleportDistance, gameObject.transform.position.y, gameObject.transform.position.z);
	}
	public void StopSignEnter(StopSign signScript)
	{
		SignGrabbed ();
		canMove = false;
		shef_Rigid.velocity = Vector2.zero;
		numberOfClksNeeded  = UnityEngine.Random.Range (minClicks,maxClicks);
		currentStopSignScript = signScript;
		uiText.UpdateText();
		gameObject.transform.position = new Vector3 (gameObject.transform.position.x + teleportDistance, gameObject.transform.position.y, gameObject.transform.position.z);
	}
	void StartCinematic()
	{
		mySpriteMainRend.sprite = propose2;
		proposedVFX.SetActive (true);
		YouWin.SetActive(true);
		gameObject.GetComponent<AudioSource> ().PlayOneShot (sparkle);
	}

}
