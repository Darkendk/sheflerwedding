﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
	bool canrestart = false;
	public Text clicktorestart;
	public Text mainText;
	public Image backgoung;
	public float fadeinspeed;
	public float uitxtindelay;
	public Text credits;
	float alhpa;
	float alhpa2;
	// Use this for initialization
	void Start ()
	{
		mainText.color = new Color (mainText.color.r,mainText.color.g,mainText.color.b,0);
		backgoung.color = new Color (backgoung.color.r,backgoung.color.g,backgoung.color.b,0);
		clicktorestart.color = new Color (clicktorestart.color.r,clicktorestart.color.g,clicktorestart.color.b,0);
		credits.color = new Color (credits.color.r,credits.color.g,credits.color.b,0);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetMouseButton(0))
		{
			if (canrestart == true)
			{
				Application.LoadLevel(0);
			}
		}
		if( canrestart == true)
		{
			alhpa = clicktorestart.color.a;
			if( clicktorestart.color.a < 1)
			{
				alhpa += Time.deltaTime*fadeinspeed;
				clicktorestart.color = new Color (clicktorestart.color.r,clicktorestart.color.g,clicktorestart.color.b,alhpa);
				credits.color = new Color (credits.color.r,credits.color.g,credits.color.b,alhpa);
			}
		}

		alhpa2 = mainText.color.a;
		if( mainText.color.a < 1)
		{
			alhpa2 += Time.deltaTime*fadeinspeed;
			mainText.color = new Color (mainText.color.r,mainText.color.g,mainText.color.b,alhpa2);
			backgoung.color = new Color (255,255,255,alhpa2*0.65f);
		}
	}
	void OnEnable()
	{
		Invoke ("EnableRestart", uitxtindelay);
	}
	void EnableRestart()
	{
			canrestart = true;
	}
}
