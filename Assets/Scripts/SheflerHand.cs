﻿using UnityEngine;
using System.Collections;

public class SheflerHand : MonoBehaviour
{
	GameObject currentStopSign;
	StopSign currentStopSignScript;
	SpriteRenderer mySpriteRend;
	Animator handAnim;
	public Rigidbody2D shefRidgid;
	public float therashold;
	// Use this for initialization
	void Start ()
	{
		mySpriteRend = gameObject.GetComponent<SpriteRenderer> ();
		Shefler.Instance.Proposed += hidearm;
		Shefler.Instance.SignGrabbed += Grabsign;
		Shefler.Instance.SignDestroied += ResetSprite;
		handAnim = gameObject.GetComponent<Animator> ();

	}
	void Update()
	{
		if (shefRidgid.velocity.x < therashold) 
		{
			handAnim.SetBool("FlapArms",true);
		}
		else 
		{
			handAnim.SetBool("FlapArms",false);
		}
	}
	void Grabsign()
	{
		handAnim.SetBool("Grab",true);
		mySpriteRend.sortingOrder = 3;
	}
	void ResetSprite()
	{
		handAnim.SetBool("Grab",false);
		mySpriteRend.sortingOrder = -1;
	}
	void hidearm()
	{
		mySpriteRend.enabled = false;
	}

	void OnDestroy()
	{
		if(GameObject.FindObjectOfType<Shefler>())
		{
			Shefler.Instance.SignGrabbed -= hidearm;
			Shefler.Instance.SignGrabbed -= Grabsign;
			Shefler.Instance.SignDestroied -= ResetSprite;
		}
	}
}
