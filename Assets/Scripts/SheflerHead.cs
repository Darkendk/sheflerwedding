﻿using UnityEngine;
using System.Collections;

public class SheflerHead : MonoBehaviour 
{
	SpriteRenderer mySpriteRend;
	Animator headAnim;
	public Rigidbody2D shefRidgid;
	public float therashold;
	// Use this for initialization
	void Start () 
	{
		mySpriteRend = gameObject.GetComponent<SpriteRenderer> ();
		Shefler.Instance.Proposed += hidehead;
		Shefler.Instance.TriedToMove += FastNode;
		headAnim = gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (shefRidgid.velocity.x < therashold) 
		{
			headAnim.SetBool("HeadShake",true);
		}
		else 
		{
			headAnim.SetBool("HeadShake",false);
		}
	}
	void hidehead()
	{
		mySpriteRend.enabled = false;
	}
	void OnDestroy()
	{
		if(GameObject.FindObjectOfType<Shefler>())
		{
			Shefler.Instance.SignGrabbed -= hidehead;
			Shefler.Instance.TriedToMove -= FastNode;
		}
	}
	void FastNode()
	{
		headAnim.SetBool("TryToMove",true);
		Invoke ("StopFastNode", 1f);
	}
	void StopFastNode()
	{
		headAnim.SetBool("TryToMove",false);
	}
}
