﻿using UnityEngine;
using System.Collections;

public class SheflerSignTrigger : MonoBehaviour
{
	GameObject currentStopSign;
	StopSign currentStopSignScript;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D(Collider2D col)
	{
		if(gameObject.tag == "Shefler")
		{
			if(col.gameObject.tag == "StopSign")
			{
				currentStopSign = col.gameObject;
				currentStopSignScript = currentStopSign.GetComponent<StopSign>();
				Shefler.Instance.StopSignEnter(currentStopSignScript);
			}
		}
	}
}
