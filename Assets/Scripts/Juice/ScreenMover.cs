﻿using UnityEngine;
using System.Collections;

public class ScreenMover : MonoBehaviour
{
	public Rigidbody2D myRigidbody;
	//Vector3 bldui_tartingPos;
	//public float moveFWDSpeed = 1;
	//public float moveBackSpeed = 2.7f;
	public MovableElement[] objs;
	public float screenlimiter = -2;
	public GameObject buildings; // This is For the Paralex Effec
	bool swiping;
	Vector3 firstPosition;
	Vector3 swipeStartPos;
	bool swipeStart = false; // this bool will help to get the position on first touch and apply the limiter on the screen.

	// Use this for initialization
	void Start ()
	{
		//bldui_tartingPos = buildings.transform.position;
		Shefler.Instance.SignGrabbed += ResetObjs;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (Input.GetMouseButtonDown (0))
		{
			if(Shefler.Instance.canMove == false)
			{
				ScreenShaker.Instance.Shake(0.05f,0.07f);
			}
		}

		if (Input.GetMouseButton(0))
		{
			if (swiping == false)
			{
				swiping = true;
				firstPosition = Input.mousePosition;
				return;
			}
			else
			{
					Vector3 direction = Input.mousePosition - firstPosition;
					if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
					{
						if (direction.x > 0) 
						{
							
						}
						else
						{ 	
							if(Shefler.Instance.canMove == false)
							{
								ScreenShaker.Instance.Shake(0.1f,0.1f);
							}
							else if((transform.position.x  - swipeStartPos.x)> screenlimiter)
							{
								MoveForwardObjects();
							}
						}
					}
			}
			swiping = false;
			if(swipeStart == false)
			{
				swipeStart = true;
				swipeStartPos = gameObject.transform.position;
			}
		
		}
		else 
		{
			if(Shefler.Instance.canMove == true) // This is to allow the Final Sign ScreenShake
			{
				MoveBackwordsObjects();
			}
		}

		if (Input.GetMouseButtonUp (0))
		{
			swipeStart = false;
		}
	
	}
	void MoveForwardObjects()
	{
		foreach (MovableElement objectToMove in objs) 
		{
			objectToMove.obj.transform.position = new Vector3 (objectToMove.transform.position.x - Time.deltaTime*objectToMove.fWDSpeed,objectToMove.transform.position.y,objectToMove.transform.position.z);
		}
	}
	void ResetObjs()
	{
		foreach (MovableElement objectToMove in objs) 
		{
			objectToMove.obj.transform.position = objectToMove.startPos;
		}
	}
	void MoveBackwordsObjects()
	{
		foreach (MovableElement objectToMove in objs) 
		{
			if((objectToMove.obj.transform.position.x - objectToMove.startPos.x) < -0.01f)
			{
				objectToMove.obj.transform.position = new Vector3 (objectToMove.transform.position.x + Time.deltaTime*objectToMove.bkSpeed,objectToMove.transform.position.y,objectToMove.transform.position.z);
			}
		}
	}
	void OnDestroy()
	{
		if(GameObject.FindObjectOfType<Shefler>())
		{
			Shefler.Instance.SignGrabbed -= ResetObjs;
		}
	}
}
