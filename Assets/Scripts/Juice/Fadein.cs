﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Fadein : MonoBehaviour
{
	bool startfade = false;
	public Image img;
	public float fadeinspeed;
	public float uitxtindelay;
	float alhpa;
	// Use this for initialization
	void Start ()
	{
		img.color = new Color (255,255,255,0);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if( startfade == true)
		{
			alhpa = img.color.a;
			if( img.color.a < 1)
			{
				alhpa += Time.deltaTime*fadeinspeed;
				img.color = new Color (255,255,255,alhpa);
			}
		}
	}

	void OnEnable()
	{
		Invoke ("EnableRestart", uitxtindelay);
	}
	void EnableRestart()
	{
		startfade = true;
	}
}

