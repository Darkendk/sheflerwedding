﻿using UnityEngine;
using System.Collections;

public class ParalexBG : MonoBehaviour {

	Rigidbody2D myRigidbody;
	Vector3 startingPos;
	float accs;
	public float moveFWDSpeed = 2;
	public float moveBackSpeed = 2.7f;
	public float screenlimiter = -2;
	float therashold = -0.2f;
	bool swiping;
	Vector3 firstPosition;
	Vector3 swipeStartPos;
	
	// Use this for initialization
	void Start ()
	{
		startingPos = gameObject.transform.position;
		myRigidbody = Shefler.Instance.gameObject.GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update ()
	{

		if (myRigidbody.velocity.x < therashold) 
		{
			if((transform.position.x  - swipeStartPos.x)> screenlimiter)
			{
				transform.position = new Vector3 (transform.position.x - Time.deltaTime*moveFWDSpeed,transform.position.y,transform.position.z);
			}
		}
		else
		{
			if ((transform.position.x - startingPos.x) < 0.01f)
			{
				transform.position = new Vector3 (transform.position.x + Time.deltaTime*moveBackSpeed,transform.position.y,transform.position.z);
			}
		}
	
			if(Shefler.Instance.canMove == false)
			{
				transform.position = startingPos;
			}
	}
}
