﻿using UnityEngine;
using System.Collections;

public class PulseFade : MonoBehaviour
{
	bool fadein = true;
	public SpriteRenderer mysprite;
	public float fadeinspeed;
	public float uitxtindelay;
	float alhpa;
	// Use this for initialization
	void Start ()
	{
		mysprite = gameObject.GetComponent<SpriteRenderer> ();
		alhpa = 0;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if( fadein == true)
		{
			if( mysprite.color.a < 1)
			{
				alhpa += Time.deltaTime*fadeinspeed;
				mysprite.color = new Color (255,255,255,alhpa);
			}
			else
			{
				fadein = false;
			}
		}
		else
		{
			if( mysprite.color.a > 0.01f)
			{
				alhpa -= Time.deltaTime*fadeinspeed;
				mysprite.color = new Color (255,255,255,alhpa);
			}
			else
			{
				fadein = true;
			}
		}
	}
}
