﻿using UnityEngine;
using System.Collections;

public class StopSign : MonoBehaviour
{
	public Sprite brokenSprite;
	SpriteRenderer mysprite;
	BoxCollider2D myCol;
	AudioSource myAudioSource;
	public AudioClip[] grabSign;
	public AudioClip[] signBreak;
	public AudioClip[] holdOnToSign;
	public GameObject[] prticles;
	float grabbingpitch;
	float minPitch = 0.7f;
	float maxPitch = 1.3f;
	// Use this for initialization
	void Start ()
	{
		mysprite = gameObject.GetComponent<SpriteRenderer> ();
		myCol = gameObject.GetComponent<BoxCollider2D> ();
		myAudioSource = gameObject.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.tag == "Shefler")
		{
			grabbingpitch = Random.Range(minPitch,maxPitch);
			myAudioSource.pitch = grabbingpitch;
			myAudioSource.PlayOneShot(grabSign[0]);
			if(Tutorial.Instance.arrow.activeSelf == true)
			{
				Tutorial.Instance.arrow.SetActive(false);
			}
			else if(Tutorial.Instance.count == true)
			{
				Tutorial.Instance.count = false;
				Tutorial.Instance.arrow.SetActive(false);
			}
		}
	}
	public void DestroyMe()
	{
		myCol.enabled = false;
		mysprite.sprite = brokenSprite;
		print (gameObject.name);
		myAudioSource.pitch = grabbingpitch;
		myAudioSource.PlayOneShot(signBreak[0]);
		if (prticles.Length > 0) 
		{ 
			foreach (GameObject particle in prticles) 
			{
				particle.SetActive(true);
			}
		}
	}
	public void HoldSFX()
	{
		myAudioSource.pitch = Random.Range(minPitch,maxPitch);
		myAudioSource.PlayOneShot(holdOnToSign[0]);
	}
}
