﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TextDisplay : MonoBehaviour
{

	public Text clksToClick;
	public Text clksClicked;
	public Text clickstodisplay;
	Shefler shefler_Script;
	int clicksleft;
	float ratio;
	// Use this for initialization
	void Start ()
	{
		shefler_Script = gameObject.GetComponent < Shefler> ();
	}
	

	public void UpdateText()
	{
		clicksleft = shefler_Script.numberOfClksNeeded - shefler_Script.numberOfClks;
		//ratio  = 255 / shefler_Script.numberOfClksNeeded;
		//clickstodisplay.color = new Color ((ratio*clicksleft)/255 , (ratio*shefler_Script.numberOfClks)/255, 0);
		//clksToClick.text = ("Clicks To Click: " + shefler_Script.numberOfClksNeeded.ToString ());
		//clksClicked.text = ("Clicks Clicked: " + shefler_Script.numberOfClks.ToString());
		clickstodisplay.text = (clicksleft.ToString());
	}

	public void ResetText()
	{
		//clksToClick.text = ("Clicks To Click: 0");
		//clksClicked.text = ("Clicks Clicked: 0");
		clickstodisplay.text = ("");
	}
}
