﻿using UnityEngine;
using System.Collections;

public class VoiceNoTimer : MonoBehaviour
{
	AudioSource myAudio;
	public AudioClip VoiceNo;
	public int minTime;
	public int MaxTime;
	float timer;
	float timeAfterPlayed;
	float minPitch = 1.5f;
	float maxPitch = 2f;
	// Use this for initialization
	void Start ()
	{
		myAudio = gameObject.GetComponent<AudioSource> ();
		timeAfterPlayed = Random.Range(minTime,MaxTime);
	}
	
	// Update is called once per frame
	void Update ()
	{
		timer += Time.deltaTime;
		if(timer > timeAfterPlayed)
		{
			myAudio.pitch = Random.Range(minPitch,maxPitch);
			myAudio.PlayOneShot(VoiceNo);
			timer = 0;
			timeAfterPlayed = Random.Range(minTime,MaxTime);
		}
	}
}
