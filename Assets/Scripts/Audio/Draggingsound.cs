﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;

public class Draggingsound : MonoBehaviour
{
	Rigidbody2D shefRidgid;
	AudioSource myaudio;
	public float therashold;
	public float fadeMultiplayer;
	public ParticleSystem[] particles;
	bool Particleon = false;
	// Use this for initialization
	void Start ()
	{
		shefRidgid = gameObject.GetComponent<Rigidbody2D> ();
		myaudio = gameObject.GetComponent<AudioSource> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (shefRidgid.velocity.x < therashold) 
		{
			myaudio.volume = -shefRidgid.velocity.x*0.9f;
			if(Particleon == false)
			{
				Particleon = true;
				ToggleParticles(Particleon);
			}

			foreach (ParticleSystem part in particles)
			{
				part.maxParticles = Mathf.RoundToInt(-shefRidgid.velocity.x*10);
			}
		}
		else 
		{
			if(myaudio.volume > 0)
			{
				myaudio.volume -= Time.deltaTime*fadeMultiplayer;
				if(Shefler.Instance.canMove == false)
				{
					myaudio.volume -= Time.deltaTime*1.5f;
				}
				if(Particleon == true)
				{
					Particleon = false;
					ToggleParticles(Particleon);
				}
				foreach (ParticleSystem part in particles)
				{
					part.maxParticles = 0;
				}
			}
		}
	}
	void ToggleParticles(bool toggle)
	{

			foreach (ParticleSystem part in particles)
			{
				if(toggle == true)
				{
					part.Play();
				}
				if(toggle == false)
				{
					part.Stop();
				}

			}
	}
}
