﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour
{
	public AudioSource[] musicLayers;
	int musicIntensity = 0;	// the music Intensity 
	bool changeIntensity = true; // a bool that will determine if the states are changing or not
	public float fadeSpeed = 1;
	// Use this for initialization
	void Start ()
	{
		if(musicLayers.Length > 0) // if the array music Layer is not empty 
		{
			foreach (AudioSource audiosource in musicLayers) // Go through music layers array 
			{
				audiosource.Play(); // Play all the Intensity levels * it is very important that the Play() function will be called for all the audio sources on the same frame, makes sure that they will stay in Sync. 
				audiosource.volume = 0; // Mute all the Intensity levels 
			}
		}
		else
		{
			Debug.LogError("musicLayers array is empty"); // Present an error message when the array is empty 
		}
		Shefler.Instance.SignGrabbed += SignedEntered;
		Shefler.Instance.SignDestroied += SignedExited;
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(changeIntensity == true) //if changeIntensity is set to true start changing Intensity
		{
			if(musicLayers[musicIntensity].volume < 1) // as long as the desired intensity audio source lower than 100% volume call Change layer function
			{
				ChangeLayer(musicIntensity);
			}
			else
			{
				changeIntensity = false; // when the volume have reached 100% stop changeIntensity is false and now it will not call the Change layer function
			}
		}

	}
	void ChangeLayer(int changeTo)
	{
		for (int i = 0; i < musicLayers.Length; i++) 
		{
			if(i == changeTo)
			{
				musicLayers[i].volume += Time.deltaTime*fadeSpeed; // add volume to the desired Intensity layer
			}
			else
			{
				musicLayers[i].volume -= Time.deltaTime*fadeSpeed; // lower volume from all other Intensity layers
			}
			// this allows us to have smooth transitions because the same amount that is add to the desired Intensity layer is lowered all the other ones.
		}
	}

	void SignedEntered()
	{
		changeIntensity = true;
		musicIntensity = 1;
	}
	void SignedExited()
	{
		changeIntensity = true;
		musicIntensity = 0;
	}

	void OnDestroy()
	{
		if(GameObject.FindObjectOfType<Shefler>())
		{
			Shefler.Instance.SignGrabbed -= SignedEntered;
			Shefler.Instance.SignDestroied -= SignedExited;
		}
	}
}
