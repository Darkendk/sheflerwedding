﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Yam : MonoBehaviour {

	float timeTobeOut = 5;
	public float timeTobeOutMin = 3;
	public float timeTobeOutMax = 7;
	float timer;
	public bool visable = true;
	Touch[] mytouches;
	Collider2D mycol;
	RaycastHit hit;
	public AudioClip yes;
	public AudioClip no;
	public Text displayTimerYam;
	public Sprite proposedExited;
	public AudioClip yamqueel;
	public AudioClip yamGoneHome;
	public AudioClip yamcomeback;
	public AudioSource yamSFXAudioSource;
	public AudioSource gameoverAudioSource;
	public float pitchmix = 1.3f;
	public float pitchmax = 1.7f;
	public GameObject yamIsgone;
	Animator myanim;
	bool tapYamit = false;
	// Use this for initialization

	[SerializeField] private float shakeTime = 1f;
	[SerializeField] private float shakeAmount = 0.7f;
	[SerializeField] private float decreaseFactor = 1.0f;
	
	private Vector3 originalPos;
	void Start () 
	{
		mycol = GetComponent<Collider2D> ();
		originalPos = transform.position;
		myanim = gameObject.GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		if(Shefler.Instance.gameoverbool == false)
		{
			if (visable == true)
			{
				timer += Time.deltaTime;
				if(timer > timeTobeOut)
				{
					myanim.SetBool("GoHome",true);
					visable = false;
					displayTimerYam.text = "";
					yamSFXAudioSource.pitch = Random.Range(pitchmix,pitchmax);
					yamSFXAudioSource.PlayOneShot(yamGoneHome);
					yamIsgone.SetActive (true);
				
				}
				else
				{
					displayTimerYam.text = ((timeTobeOut - timer).ToString("#.#"));
				}
			}
			if(tapYamit == false)
			{
				mytouches = Input.touches;
				foreach (Touch touch in mytouches) 
				{
					Vector3 wp = Camera.main.ScreenToWorldPoint(touch.position);
					Vector2 touchPos = new Vector2(wp.x, wp.y);
					if (mycol == Physics2D.OverlapPoint(touchPos))
					{
						if(visable == false)
						{
							YamIsHere();
							tapYamit = true;
						}
					}
				}
			}
			if(Input.GetMouseButtonUp(0))
			{
				tapYamit = false;
			}
		}
			
	}
	void YamIsHere()
	{
		yamSFXAudioSource.pitch = Random.Range(pitchmix+0.2f,pitchmax);
		yamSFXAudioSource.PlayOneShot(yamcomeback);
		visable = true;
		timer = 0;
		timeTobeOut = Random.Range (timeTobeOutMin, timeTobeOutMax);
		myanim.SetBool("GoHome",false);
		yamIsgone.SetActive (false);
	}
	void OnMouseDown()
	{
		if(visable == false)
		{
			YamIsHere();
		}
	}

	public void YouWin()
	{

		gameoverAudioSource.clip = yes;
		gameoverAudioSource.Play();
		displayTimerYam.text = "";
		myanim.SetBool("Propose",true);
	}
	public void YouLose()
	{

		gameoverAudioSource.clip = no;
		gameoverAudioSource.Play();
	}
	void Squeel()
	{
		yamSFXAudioSource.pitch = 1.5f;
		yamSFXAudioSource.PlayOneShot (yamqueel);
		myanim.SetBool("Propose",false);
	}

	private IEnumerator Shaking() 
	{
		while (shakeTime > 0f)
		{
			transform.localPosition = originalPos + Random.insideUnitSphere * shakeAmount;
			shakeTime -= Time.deltaTime * decreaseFactor;
			yield return new WaitForSeconds(0.001f);
		} 
		
		shakeTime = 0.0f;
		transform.localPosition = originalPos;
	}
	
	
	public void Shake(float time, float amount)
	{
		shakeTime = time;
		shakeAmount = amount;		
		StopAllCoroutines();
		StartCoroutine(Shaking());
	}
}
